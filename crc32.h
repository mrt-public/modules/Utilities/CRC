/**
  *@file crc32.h
  *@brief header for crc32 utility
  *@author Jason Berger
  *@date 07/20/23
  */

#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Exported types ------------------------------------------------------------*/

typedef struct
{
  /* data */
  uint32_t crc; //current crc value
  uint32_t len; //length of data
} crc32_ctx_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
 * @brief Calculate the crc32 of a data buffer
 * 
 * @param data ptr to data 
 * @param len length of data
 * @return uint32_t 
 */
uint32_t crc32(uint8_t* data, uint32_t len);

/**
 * @brief Construct a new crc32 context
 * 
 * @param ctx 
 */
void crc32_init(crc32_ctx_t* ctx);

/**
 * @brief Construct a new crc32 feed object
 * 
 * @param ctx ptr to crc32 context 
 * @param data ptr to data to feed into crc 
 * @param len number of bytes in data 
 */
void crc32_update(crc32_ctx_t* ctx, uint8_t* data, uint32_t len);

/**
 * @brief Get the crc32 result
 * 
 * @param ctx ptr to crc32 context
 * @return uint32_t final crc value
 */
uint32_t crc32_result(crc32_ctx_t* ctx);



#ifdef __cplusplus
}
#endif



